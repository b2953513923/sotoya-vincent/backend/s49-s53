// Dependencies and Modules
const express = require('express');
const userController = require('../controllers/user');
const auth = require('../auth');

// destructure the auth file:
const { verify, verifyAdmin } = auth;


const router = express.Router();



// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for user authentication
router.post("/login", userController.loginUser);

// Route to get user details
router.get("/details", verify, userController.getProfile);

// Route for Updating a user as an Admin
router.put('/updateAdmin', verify, verifyAdmin, userController.updateUserAdminStatus);

module.exports = router;