// router
// Dependencies and Modules
const express = require('express');
const orderController = require('../controllers/order');
const auth = require('../auth');

// destructure the auth file:
const { verify, verifyAdmin } = auth;


const router = express.Router();


// Route to purchase a product.
router.post("/purchase", verify,orderController.purchaseProduct);

// Route for retrieving all the orders
router.get('/all-orders', orderController.getAllOrders);

// Router to get the orders of a user
router.get("/:userId", verify, orderController.getUserOrders);






// Allows us to export the "router" object
module.exports = router; 