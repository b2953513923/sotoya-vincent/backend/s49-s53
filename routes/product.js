const express = require('express');
const productController = require('../controllers/product');
const auth = require('../auth');


const { verify, verifyAdmin } = auth;

const router = express.Router();


// Create a product POST - Create
router.post("/", verify, verifyAdmin,productController.addProduct);


// Route for retrieving all the products
router.get("/all", productController.getAllProducts);


// Route for retrieving all the ACTIVE products for all the users.
router.get("/", productController.getAllActive);

// Route to search for products by product name
router.post('/search', productController.searchProductsByName);

// Route for retrieving a specific product
router.get("/:productId", productController.getProduct);

// Route for updating a product (Admin) 
router.put("/:productId", verify, verifyAdmin, productController.updateProduct);

// Route for Archiving Product
router.put("/:productId/archive", verify, verifyAdmin, productController.archiveProduct);


// Route for Activating a Product
router.put("/:productId/activate", verify, verifyAdmin, productController.activateProduct);


module.exports = router;