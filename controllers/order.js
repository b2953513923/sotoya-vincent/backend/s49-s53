const Order = require('../models/Order');
const Product = require('../models/Product');
const User = require('../models/User');

// Controller function to purchase a product
// module.exports.purchaseProduct = async (req, res) => {
//     try {
//     // Check if the user is an admin
//     if (req.user.isAdmin === true) {
//       return res.status(403).json({ error: 'Admins are not allowed to order' });
//     }

//     // Extract necessary information from the request
//     const { productId, quantity } = req.body;
//     const userId = req.user.id;

//     // Fetch the product price from the database
//     const product = await Product.findById(productId);
//     if (!product) {
//       return res.status(404).json({ error: 'Product not found' });
//     }

//     const price = product.price;
//     const totalAmount = price * quantity;

//     // Create a new order
//     const order = new Order({
//       userId,
//       products: [{ productId, quantity, price }],
//       totalAmount
//     });

//     // Save the order in the database
//     await order.save();

//     return res.status(200).json({
//       message: 'Purchase Successful',
//       totalAmount
//     });
//   } catch (error) {
//     console.error(error);
//     return res.status(500).json({ error: 'Internal server error' });
//   }
// };
// [MINIMUM REQUIREMENTS] Controller function to purchase a product
module.exports.purchaseProduct = async (req, res) => {
  try {
    const { productId, quantity } = req.body;
    const userId = req.user.id;

    const user = await User.findById(userId);
    if (user.isAdmin) {
      return res.status(403).json(false);
    }

    const product = await Product.findById(productId);
    if (!product) {
      return res.status(404).json(false);
    }

    const price = product.price;
    const totalAmount = price * quantity;

    const order = new Order({
      userId,
      products: [{ productId, quantity, price }],
      totalAmount
    });

    await order.save();

    return res.json(true);
  } catch (error) {
    console.error(error);
    return res.status(500).json({ message: err.message });
  }
};

// Retrieve a user's order
module.exports.getUserOrders = async (req, res) => {
  try {

    const { userId } = req.params;
    const orders = await Order.find({ userId });

    res.status(200).json(orders);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal server error' });
  }
};

// Retrieve all orders of users
module.exports.getAllOrders = (req, res) => {
    return Order.find({}).then(result => {
        // console.log(result)
        return res.send(result);
    })
    .catch(err => res.send(err))
};


