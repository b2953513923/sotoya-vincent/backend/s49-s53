// Dependencies and Modules
const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require('bcrypt');
const auth = require('../auth');


// Register a user to the database
module.exports.registerUser = (reqBody) => {

    let newUser = new User({
        fullName: reqBody.fullName,
        email: reqBody.email,
        password: bcrypt.hashSync(reqBody.password, 10)
    })

    return newUser.save().then((user, error) => {
        if(error) {
            return false;
        } else {
            return true;
        }
    }).catch(err => err)
};


// Authenticate/login a user
module.exports.loginUser = (req, res) => {
	return User.findOne({email: req.body.email}).then(result => {
		if(result == null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(req.body.password, result.password);

			if(isPasswordCorrect) {
				return res.send({access: auth.createAccessToken(result)})
			} else {
				// If password is incorrect
				return res.send(false);
			}
		}
	}).catch(err => res.send(err));
};


// Retrieve user details
module.exports.getProfile = (req, res) => {
    return User.findById(req.user.id).then(result => {

            result.password = "";

            return res.send(result); 

        }).catch(err => res.send(err));
};


// Update user admin status
module.exports.updateUserAdminStatus = async (req, res) => {
  const { userId } = req.body;

  try {
    // Find the user to update
    const user = await User.findById(userId);

    if (!user) {
      return res.status(404).json({ error: 'User not found.' });
    }

    // Update the admin status
    user.isAdmin = true;
    await user.save();

    res.json({ message: 'User admin status updated successfully.' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Server error.' });
  }
};


