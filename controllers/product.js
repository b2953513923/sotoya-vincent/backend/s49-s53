const Product = require("../models/Product");
const User = require("../models/User")



// Add a Product
module.exports.addProduct = (req, res) => {

    let newProduct = new Product({
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    })

    
    return newProduct.save().then((product, error) => {

        if(error) {
            return res.send(false);

        } else {
            return res.send(true);
        }

    }).catch(err => res.send(err));
};



// Retrieve all Products
module.exports.getAllProducts = (req, res) => {
    return Product.find({}).then(result => {
        // console.log(result)
        return res.send(result);
    })
    .catch(err => res.send(err))
};


// Retrieve All Active Products
module.exports.getAllActive = (req, res) => {
    return Product.find({ isActive: true }).then(result => {
        // console.log(result)
        return res.send(result)
    })
    .catch(err => res.send(err))
};


// Retrieve a Specific Product
module.exports.getProduct = (req, res) => {
    return Product.findById(req.params.productId).then(result => {
        return res.send(result)
    })
    .catch(err => res.send(err))
};


// Update Product Information (Admin only)
module.exports.updateProduct = (req, res) => {

    let updatedProduct = {
        name: req.body.name,
        description: req.body.description,
        price: req.body.price
    }


    return Product.findByIdAndUpdate(req.params.productId, updatedProduct)
        .then((product, error) => {

        // Product is not updated
        if(error) {
            return res.send(false);
        // Product updated successfully
        } else {
            return res.send(true);
        }

    }).catch(err => res.send(err))
};


// Archive Product (Admin Only)
module.exports.archiveProduct = (req, res) => {

    let archivedProduct = {
        isActive: false
    }
    return Product.findByIdAndUpdate(req.params.productId, archivedProduct)
    .then((product, error) => {

        if(error) {
            return res.send(false);
        } else {
            return res.send(true);
        }

    }).catch(err => res.send(err))
};


// Activate Product (Admin Only)
module.exports.activateProduct = (req, res) => {

    let activatedProduct = {
        isActive: true
    }

    return Product.findByIdAndUpdate(req.params.productId, activatedProduct)
    .then((product, error) => {

        if(error) {
            return res.send(false);
        } else {
            return res.send(true);
        }

    }).catch(err => res.send(err))
};

// Search for products by product name
module.exports.searchProductsByName = async (req, res) => {
  try {
    const { productName } = req.body;

    // Use a regular expression to perform a case-insensitive search
    const products = await Product.find({
      name: { $regex: productName, $options: 'i' }
    });

    res.json(products);
  } catch (error) {
    console.error(error);
    res.status(500).json({ error: 'Internal Server Error' });
  }
};

